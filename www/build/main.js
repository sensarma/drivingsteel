webpackJsonp([16],{

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataProvider = /** @class */ (function () {
    function DataProvider(http) {
        this.http = http;
        console.log('Hello DataProvider Provider');
    }
    DataProvider.prototype.setItemsToSearch = function (items) {
        this.items = items;
    };
    DataProvider.prototype.speakerFilterItems = function (searchTerm) {
        if (!searchTerm)
            return this.items;
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            return this.items.filter(function (item) {
                return item.post_title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    };
    DataProvider.prototype.delegatesFilterItems = function (searchTerm) {
        if (!searchTerm)
            return this.items;
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            return this.items.filter(function (item) {
                return item.display_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    };
    DataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], DataProvider);
    return DataProvider;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, navParams, restProvider, loading, screenOrientation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.loading = loading;
        this.screenOrientation = screenOrientation;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getAboutUs();
        });
    }
    AboutPage.prototype.getAboutUs = function () {
        var _this = this;
        this.restProvider.getAboutusDetails()
            .then(function (data) {
            _this.responseData = data;
            _this.genesis = _this.responseData.data.genesis;
            _this.landScape = _this.responseData.data.land_scape;
            console.log(_this.genesis);
            _this.loader.dismiss();
        });
    };
    AboutPage.prototype.ionViewDidLoad = function () {
    };
    AboutPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    AboutPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__index_index__["a" /* IndexPage */]);
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/about/about.html"*/'<!--\n  Generated template for the AboutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>about</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>About Driving Steel</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section>\n    <div class="container">\n        <div class="about_section">\n            \n            <h5>Genesis:</h5>\n            <p>{{genesis}}</p>\n\n            <h5>Land Scape: </h5>\n            <p>{{landScape}}</p>\n        </div>\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/about/about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PregistrationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { IndexPage } from '../index/index';



var PregistrationPage = /** @class */ (function () {
    function PregistrationPage(navCtrl, navParams, restProvider, toastCtrl, loading, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.screenOrientation = screenOrientation;
        this.profileUpdateData = { data: { first_name: '', last_name: '', user_email: '', password: '', repassword: '', mobile: '', } };
        this.imageUpdateData = { data: { id: '', profile_image: '' } };
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.group = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            firstName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required]),
            lastName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required]),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].email]),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6)]),
            repassword: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6)])
        });
    }
    PregistrationPage.prototype.doSignUp = function () {
        var _this = this;
        console.log(this.profileUpdateData);
        if (this.profileUpdateData.data.password == this.profileUpdateData.data.repassword) {
            var loader_1 = this.loading.create({
                content: 'Please Wait...',
            });
            loader_1.present().then(function () {
                _this.restProvider.SignUp(_this.profileUpdateData).then(function (result) {
                    _this.dataResult = result;
                    loader_1.dismiss();
                    _this.presentToast(_this.dataResult.message);
                    console.log(_this.dataResult);
                    if (_this.dataResult.success == true)
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                }, function (err) {
                    loader_1.dismiss();
                    _this.presentToast(err);
                    console.log(err);
                });
            });
        }
        else {
            this.presentToast("Please verify password");
        }
    };
    PregistrationPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    PregistrationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PregistrationPage');
    };
    PregistrationPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    PregistrationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pregistration',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/pregistration/pregistration.html"*/'<!--\n  Generated template for the PregistrationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="gotoIndex()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Sign Up</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section class="green_bg">a\n    <div class="profile_edit">\n        <div class="container">\n            <form class="row" >\n                <!-- <div class="profile_img col-md-12">\n                    <img data-src="{{profileUpdateData.data.profile_img}}"\n                         onError="this.src=\'../assets/images/profile-img.jpg\';"alt="">\n                    <h3>Profile Photo</h3>\n                     <div class="profile_img col-md-12">\n                        <button [disabled]="!imageUpdateData.data.profile_image" (click)="uploadFile()">Upload</button>\n                        <button (click)="getImage()">/ Edit</button>\n                    </div>\n                </div> -->\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="First Name" placeholder="First Name" class="form-control" [(ngModel)]="profileUpdateData.data.first_name" formControlName="firstName">\n                        <span class="error" *ngIf="group.get(\'firstName\').hasError(\'required\') && group.get(\'firstName\').touched" style="margin-top: 10px;margin-left: 5px">\n                            First Name is required\n                        </span>\n\n                </div>\n\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="Last Name" placeholder="Last Name" class="form-control" [(ngModel)]="profileUpdateData.data.last_name" formControlName="lastName">\n                        <span class="error" *ngIf="group.get(\'lastName\').hasError(\'required\') && group.get(\'lastName\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Last Name is required\n                        </span>\n\n                </div>\n\n                 <div class="form-group col-md-6" >\n                    <input type="text" name="Mobile" placeholder="Mobile +91" class="form-control" [(ngModel)]="profileUpdateData.data.mobile">                     \n                </div> \n                <!-- <div class="form-group col-md-6" >\n                    <input type="text" name="phone" placeholder="phone" class="form-control" [(ngModel)]="profileUpdateData.data.phone">\n                </div > -->\n\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="Email" placeholder="Email" class="form-control" [(ngModel)]="profileUpdateData.data.user_email" formControlName="email">\n                        <span class="error" *ngIf="group.get(\'email\').hasError(\'required\') && group.get(\'email\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Email is required\n                        </span>\n                        <span class="error" *ngIf="group.get(\'email\').hasError(\'email\') && group.get(\'email\').touched" style="margin-top: 10px;margin-left: 5px">\n                          Please enter valid email\n                        </span>\n\n                </div>\n               <!--  <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="Company" placeholder="Company Name" class="form-control" [(ngModel)]="profileUpdateData.data.company_name" formControlName="companyName">\n                        <span class="error" *ngIf="group.get(\'companyName\').hasError(\'required\') && group.get(\'companyName\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Company Name is required\n                        </span>\n\n                </div> -->\n\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="Password" placeholder="Password" class="form-control" [(ngModel)]="profileUpdateData.data.password" formControlName="password">\n                   		<span class="error" *ngIf="group.get(\'password\').hasError(\'required\') && group.get(\'password\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Password is required\n                        </span>\n                        \n                        <span class="error" *ngIf="group.get(\'password\').hasError(\'minlength\') && group.get(\'password\').touched" style="margin-top: 10px;margin-left: 5px">\n                          Password must be at least 6 characters long.\n                        </span>\n\n                </div>\n\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="RePassword" placeholder="Re-enter Password" class="form-control" [(ngModel)]="profileUpdateData.data.repassword"\n                    formControlName="repassword">\n\n                    	<span class="error" *ngIf="group.get(\'repassword\').hasError(\'required\') && group.get(\'repassword\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Password is required\n                        </span>\n                        \n                        <span class="error" *ngIf="group.get(\'repassword\').hasError(\'minlength\') && group.get(\'repassword\').touched" style="margin-top: 10px;margin-left: 5px">\n                          Password must be at least 6 characters long.\n                        </span>\n\n                </div>\n\n                <!-- <div class="form-group col-md-12">\n                    <label>My Linkedin Profile</label>\n                    <input type="text" name="LinkedIn" placeholder="Paste Address" class="form-control" [(ngModel)]="profileUpdateData.data.linkedin" style="margin-top: 5px;">\n                </div> -->\n                <!-- <div class="form-group col-md-12" [formGroup]="group">\n                    <label class="checkbox">\n                        <input type="checkbox" name="checkTerms" formControlName="termsAccepted">\n                        <span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</span>\n\n                    </label>\n                </div> -->\n                <div class="profile_btns col-md-12">\n                    <button type="submit" class="save_btn" (click)="doSignUp();" [disabled]="group.invalid">Submit</button>\n                    <button type="submit" class="cancel_btn" (click)="gotoIndex();">Cancel</button>\n                </div>\n            </form>\n        </div>\n    </div>\n    \n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/pregistration/pregistration.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], PregistrationPage);
    return PregistrationPage;
}());

//# sourceMappingURL=pregistration.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, restProvider, toastCtrl, loading, storage, transfer, camera, screenOrientation, base64) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.storage = storage;
        this.transfer = transfer;
        this.camera = camera;
        this.screenOrientation = screenOrientation;
        this.base64 = base64;
        this.profileUpdateData = { data: { id: '', profile_img: '', first_name: '', last_name: '', user_email: '', password: '', repassword: '', mobile: '', phone: '', company_name: '', linkedin: '' } };
        this.imageUpdateData = { data: { id: '', profile_image: '' } };
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.group = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            firstName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required]),
            // lastName : new FormControl('',[Validators.required]),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].email]),
            termsAccepted: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */](false, [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required])
        });
        this.storage.get('user').then(function (result) {
            _this.userSession = result;
            _this.oldImageUrl = _this.userSession.profile_img;
            _this.imageUpdateData.data.id = _this.userSession.id;
            _this.profileUpdateData.data.profile_img = _this.userSession.profile_img;
            _this.profileUpdateData.data.id = _this.userSession.id;
            _this.profileUpdateData.data.first_name = _this.userSession.first_name;
            _this.profileUpdateData.data.last_name = _this.userSession.last_name;
            _this.profileUpdateData.data.user_email = _this.userSession.user_email;
            _this.profileUpdateData.data.mobile = _this.userSession.mobile;
            _this.profileUpdateData.data.phone = _this.userSession.phone;
            _this.profileUpdateData.data.company_name = _this.userSession.company_name;
            _this.profileUpdateData.data.linkedin = _this.userSession.linkedin;
            console.log("get image " + _this.oldImageUrl);
        });
    }
    ProfilePage.prototype.doProfileUpdate = function () {
        var _this = this;
        console.log(this.profileUpdateData);
        if (this.profileUpdateData.data.password == this.profileUpdateData.data.repassword) {
            this.validation();
            if (this.isvalid == true) {
                var loader_1 = this.loading.create({
                    content: 'Please Wait...',
                });
                loader_1.present().then(function () {
                    _this.restProvider.profileUpdate(_this.profileUpdateData).then(function (result) {
                        _this.dataResult = result;
                        loader_1.dismiss();
                        _this.checkResult("DetailsUpload");
                        console.log(_this.dataResult);
                    }, function (err) {
                        loader_1.dismiss();
                        _this.presentToast(err);
                        console.log(err);
                    });
                });
            }
            else {
                this.presentToast("Password must be at least 6 characters long.");
            }
        }
        else {
            this.presentToast("Please verify password");
        }
    };
    ProfilePage.prototype.validation = function () {
        this.isvalid = true;
        if (this.profileUpdateData.data.password.length > 0) {
            if (this.profileUpdateData.data.password.length < 6)
                this.isvalid = false;
        }
    };
    ProfilePage.prototype.checkResult = function (type) {
        var _this = this;
        this.presentToast(this.dataResult.message);
        if (this.dataResult.success == true) {
            if (type == "ImageUpload") {
                this.imageUpdateData.data.profile_image = '';
                this.profileUpdateData.data.profile_img = this.dataResult.data.image_path;
                console.log("udate image path " + this.profileUpdateData.data.profile_img + " data path " + this.dataResult.data.image_path);
            }
            this.storage.set('user', this.profileUpdateData.data).then(function () {
                if (type == "DetailsUpload") {
                    //this.gotoIndex();
                    _this.group.controls['termsAccepted'].setValue(false);
                }
            });
        }
        else {
            if (type == "ImageUpload") {
                this.imageUpdateData.data.profile_image = '';
                this.profileUpdateData.data.profile_img = this.oldImageUrl;
            }
        }
    };
    ProfilePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    ProfilePage.prototype.getImage = function () {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.loader = _this.loading.create({
                content: 'Uploading...',
            });
            _this.loader.present().then(function () {
                _this.base64.encodeFile(imageData).then(function (base64File) {
                    _this.imageUpdateData.data.profile_image = base64File;
                    _this.uploadFile();
                }, function (err) {
                    console.log(err);
                    //this.presentToast(err);
                    _this.loader.dismiss();
                });
            });
        }, function (err) {
            console.log("image error " + err);
            // this.presentToast(err);
        });
    };
    ProfilePage.prototype.uploadFile = function () {
        var _this = this;
        this.restProvider.uploadProfileImage(this.imageUpdateData).then(function (result) {
            _this.dataResult = result;
            _this.loader.dismiss();
            _this.checkResult("ImageUpload");
            console.log("result image upload " + _this.dataResult);
        }, function (err) {
            _this.loader.dismiss();
            _this.presentToast("Server error occured. Please try again");
            console.log(err);
        });
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    ProfilePage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index_index__["a" /* IndexPage */]);
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/profile/profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>profile</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>My Profile</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section class="green_bg">\n    <div class="profile_edit">\n        <div class="container">\n            <form class="row" >\n                <div class="profile_img col-md-12">\n                    <img data-src="{{profileUpdateData.data.profile_img}}"\n                         onError="this.src=\'../assets/images/profile-img.jpg\';"alt="" imageViewer>\n                    <h3>Profile Photo</h3>\n                     <div class="profile_img col-md-12">\n                        <!-- <button [disabled]="!imageUpdateData.data.profile_image" (click)="uploadFile()">Upload</button> -->\n                        <button (click)="getImage()">Upload / Edit</button>\n                    </div>\n                </div>\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="First Name" placeholder="First Name" class="form-control" [(ngModel)]="profileUpdateData.data.first_name" formControlName="firstName">\n                        <span class="error" *ngIf="group.get(\'firstName\').hasError(\'required\') && group.get(\'firstName\').touched" style="margin-top: 10px;margin-left: 5px">\n                            First Name is required\n                        </span>\n\n                </div>\n                <div class="form-group col-md-6" >\n                    <input type="text" name="Last Name" placeholder="Last Name" class="form-control" [(ngModel)]="profileUpdateData.data.last_name" >\n                        <!-- <span class="error" *ngIf="group.get(\'lastName\').hasError(\'required\') && group.get(\'lastName\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Last Name is required\n                        </span> -->\n\n                </div>\n                <div class="form-group col-md-6" >\n                    <input type="text" name="Mobile" placeholder="Mobile +91" class="form-control" [(ngModel)]="profileUpdateData.data.mobile" >\n                       <!--  <span class="error" *ngIf="group.get(\'mobile\').hasError(\'required\') && group.get(\'mobile\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Mobile number is required\n                        </span>\n                        <span class="error" *ngIf="group.get(\'mobile\').hasError(\'minlength\') && group.get(\'mobile\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Mobile number must be at least 10 characters long.\n                        </span> -->\n\n                </div>\n                <!-- <div class="form-group col-md-6" >\n                    <input type="text" name="phone" placeholder="phone" class="form-control" [(ngModel)]="profileUpdateData.data.phone">\n                </div > -->\n                <div class="form-group col-md-6" [formGroup]="group">\n                    <input type="text" name="Email" placeholder="Email" class="form-control" [(ngModel)]="profileUpdateData.data.user_email" formControlName="email">\n                        <span class="error" *ngIf="group.get(\'email\').hasError(\'required\') && group.get(\'email\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Email is required\n                        </span>\n                        <span class="error" *ngIf="group.get(\'email\').hasError(\'email\') && group.get(\'email\').touched" style="margin-top: 10px;margin-left: 5px">\n                          Please enter valid email\n                        </span>\n\n                </div>\n                <div class="form-group col-md-12">\n                    <input type="text" name="Company" placeholder="Company Name" class="form-control" [(ngModel)]="profileUpdateData.data.company_name">\n                       <!--  <span class="error" *ngIf="group.get(\'companyName\').hasError(\'required\') && group.get(\'companyName\').touched" style="margin-top: 10px;margin-left: 5px">\n                            Company Name is required\n                        </span> -->\n\n                </div>\n\n                <div class="form-group col-md-6">\n                    <input type="text" name="Password" placeholder="New Password" class="form-control" [(ngModel)]="profileUpdateData.data.password">\n\n                </div>\n\n                <div class="form-group col-md-6">\n                    <input type="text" name="RePassword" placeholder="Re-enter Password" class="form-control" [(ngModel)]="profileUpdateData.data.repassword">\n\n                </div>\n\n                <div class="form-group col-md-12">\n                    <label>My Linkedin Profile</label>\n                    <input type="text" name="LinkedIn" placeholder="Paste Address" class="form-control" [(ngModel)]="profileUpdateData.data.linkedin" style="margin-top: 5px;">\n                </div>\n                <div class="form-group col-md-12" [formGroup]="group">\n                    <label class="checkbox">\n                        <input type="checkbox" name="checkTerms" formControlName="termsAccepted">\n                         <span>I have read and agree to the<a ng-href="" onclick="window.open(\'https://www.drivingsteel.com/privacy-policy/\',\'_system\',\'location=yes\');" > privacy policy</a> of Driving Steel</span>\n\n                    </label>\n                </div>\n                <div class="profile_btns col-md-12">\n                    <button type="submit" class="save_btn" (click)="doProfileUpdate();" [disabled]="group.invalid || group.get(\'termsAccepted\').value != true">Save</button>\n                    <button type="submit" class="cancel_btn" (click)="gotoIndex();">Cancel</button>\n                </div>\n            </form>\n        </div>\n    </div>\n    \n</section>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__["a" /* Base64 */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_data__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__spekar_details_spekar_details__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SpeakerPage = /** @class */ (function () {
    function SpeakerPage(navCtrl, navParams, restProvider, dataService, loading, alertCtrl, screenOrientation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.dataService = dataService;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.screenOrientation = screenOrientation;
        this.searchTerm = '';
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getSpeakers();
        });
    }
    SpeakerPage.prototype.getSpeakers = function () {
        var _this = this;
        this.restProvider.getSpeakerlist()
            .then(function (data) {
            _this.responseData = data;
            _this.speakerLists = _this.responseData.data;
            _this.arrayListSize = _this.speakerLists.length;
            _this.responseMessage = _this.responseData.message;
            _this.dataService.setItemsToSearch(_this.speakerLists);
            console.log(_this.speakerLists);
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            //this.presentToast(JSON.stringify(err));
        });
    };
    SpeakerPage.prototype.setFilteredItems = function () {
        this.speakerLists = this.dataService.speakerFilterItems(this.searchTerm);
    };
    SpeakerPage.prototype.ionViewDidLoad = function () {
    };
    SpeakerPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SpeakerPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index_index__["a" /* IndexPage */]);
    };
    SpeakerPage.prototype.gotoSdetails = function (event, speakerDetails) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__spekar_details_spekar_details__["a" /* SpekarDetailsPage */], {
            data: speakerDetails
        });
    };
    SpeakerPage.prototype.doPromptForNoLinkedinLink = function () {
        var prompt = this.alertCtrl.create({
            title: '',
            message: 'No link has been provided',
            cssClass: 'alertCustomCss',
            buttons: [
                {
                    text: 'Ok',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    SpeakerPage.prototype.openLinkedIn = function (url) {
        window.open(url, '_system', 'location=yes');
    };
    SpeakerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/speaker/speaker.html"*/'<!--\n  Generated template for the SpeakerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>speaker</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Speaker Profiles</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section class="profiles_section">\n    <div class="container">\n        <div class="profile_search">\n            <input type="text" name="" placeholder="Search" class="form-control" [(ngModel)]="searchTerm"  (input)="setFilteredItems($event)">\n            <button type="submit"  (click)="setFilteredItems()"><i class="fa fa-search" aria-hidden="true"></i></button>\n        </div>\n\n        <!-- <ion-list inset> -->\n\n          <div class="speaker_list" *ngIf="!arrayListSize || arrayListSize == 0">\n                \n\n                    <div class="name">{{responseMessage}}</div>\n\n                   \n          </div>\n\n                        \n          <div *ngIf="arrayListSize || arrayListSize > 0">\n               \n            <div *ngFor="let speakerList of speakerLists">\n        \n                <div class="speaker_list">\n\n\n                    <div class="img"><img data-src= "{{speakerList.speaker_image}}" onError="this.src=\'../assets/images/profile-img.jpg\';"alt=""></div>\n\n                    <div class="info">\n                        <div class="name">{{speakerList.post_title}}</div>\n                        <h5>{{speakerList.speaker_profession}}</h5>\n                        <h4>{{speakerList.speaker_company}}</h4>\n                        <!-- <h5><span>E-mail:</span>{{speakerList.speaker_email}}</h5> -->\n                    </div>\n                    <div class="btns">\n\n                         <div *ngIf="!speakerList.speaker_linkedin">\n                            <a (click) = "doPromptForNoLinkedinLink()" class="linkedin_btn" ><img src="../assets/images/linkedin-icon.png"></a>\n                        </div>\n\n                        <div *ngIf="speakerList.speaker_linkedin">\n                            <a  (click)="openLinkedIn(speakerList.speaker_linkedin)" class="linkedin_btn" ><img src="../assets/images/linkedin-icon.png"></a>\n                        </div>\n                        <!-- <a href="#" class="save_btn">Save Contact</a> -->\n                    </div>\n                    <p style="font-size: 12px;">{{speakerList.post_content | slice:0:200}}\n                    <span><a (click)="gotoSdetails($event,speakerList)">Read more...</a></span>\n                    </p>\n                </div>\n\n            </div>\n\n          </div>\n\n         <!--  <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n                <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n            </ion-infinite-scroll> -->\n\n\n        \n        <!-- </ion-list> -->\n\n      <!--  <div class="speaker_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <div class="name">Dr. Tim Leverton</div>\n                <h5>Head - Advanced & Product Engineering</h5>\n                <h4>Tata Motors</h4>\n                <h5><span>E-mail:</span> manas.mondal@bluehorse.in</h5>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n            <p>Dr. Tim Leverton joined Tata Motors in April 2010 as President, and Head of the Advanced and product Engineering Division. In this role he is responsible for leading the global R&D of all Tata branded passenger cars, Trucks and Buses.\n            <span><a (click)="gotoSdetails()">Read more...</a></span>\n            </p>\n        </div>\n        <div class="speaker_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <div class="name">Dr. Tim Leverton</div>\n                <h5>Head - Advanced & Product Engineering</h5>\n                <h4>Tata Motors</h4>\n                <h5><span>E-mail:</span> manas.mondal@bluehorse.in</h5>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n            <p>Dr. Tim Leverton joined Tata Motors in April 2010 as President, and Head of the Advanced and product Engineering Division. In this role he is responsible for leading the global R&D of all Tata branded passenger cars, Trucks and Buses.\n            <span><a (click)="gotoSdetails()">Read more...</a></span>\n            </p>\n        </div>\n        <div class="speaker_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <div class="name">Dr. Tim Leverton</div>\n                <h5>Head - Advanced & Product Engineering</h5>\n                <h4>Tata Motors</h4>\n                <h5><span>E-mail:</span> manas.mondal@bluehorse.in</h5>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n            <p>Dr. Tim Leverton joined Tata Motors in April 2010 as President, and Head of the Advanced and product Engineering Division. In this role he is responsible for leading the global R&D of all Tata branded passenger cars, Trucks and Buses.\n            <span><a (click)="gotoSdetails()">Read more...</a></span>\n            </p>\n        </div>-->\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/speaker/speaker.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_data_data__["a" /* DataProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], SpeakerPage);
    return SpeakerPage;
}());

//# sourceMappingURL=speaker.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpekarDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SpekarDetailsPage = /** @class */ (function () {
    function SpekarDetailsPage(navCtrl, navParams, screenOrientation, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.screenOrientation = screenOrientation;
        this.alertCtrl = alertCtrl;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.speakerDetails = navParams.get('data');
        console.log(this.speakerDetails);
    }
    SpekarDetailsPage.prototype.doPromptForNoLinkedinLink = function () {
        var prompt = this.alertCtrl.create({
            title: '',
            message: 'No link has been provided',
            cssClass: 'alertCustomCss',
            buttons: [
                {
                    text: 'Ok',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    SpekarDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpekarDetailsPage');
        console.log(this.speakerDetails);
    };
    SpekarDetailsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SpekarDetailsPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index_index__["a" /* IndexPage */]);
    };
    SpekarDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-spekar-details',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/spekar-details/spekar-details.html"*/'<!--\n  Generated template for the SpekarDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>spekarDetails</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Speaker Profiles</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section class="profiles_section">\n    <div class="container">\n        <div class="speaker_list speaker_details">\n            <div class="img"><img data-src= "{{speakerDetails.speaker_image}}" onError="this.src=\'../assets/images/profile-img.jpg\';" alt=""></div>\n            <div class="info">\n                <div class="name">{{speakerDetails.post_title}}</div>\n                <h5>{{speakerDetails.speaker_profession}}</h5>\n                <h4>{{speakerDetails.speaker_company}}</h4>\n               <!--  <h5><span>E-mail:</span>{{speakerDetails.speaker_email}}</h5> -->\n            </div>\n            <div class="btns">\n\n                 <div *ngIf="!speakerDetails.speaker_linkedin">\n                            <a (click) = "doPromptForNoLinkedinLink()" class="linkedin_btn" ><img src="../assets/images/linkedin-icon.png"></a>\n                  </div>\n\n                        <div *ngIf="speakerDetails.speaker_linkedin">\n                            <a href="{{speakerDetails.speaker_linkedin}}" class="linkedin_btn" ><img src="../assets/images/linkedin-icon.png"></a>\n                        </div>\n\n                <!-- <a href="#" class="save_btn">Save Contact</a> -->\n            </div>\n            <p style="font-size: 12px;">{{speakerDetails.post_content}}</p>\n           <!--  <p>Dr. Tim Leverton joined Tata Motors in April 2010 as President, and Head of the Advanced and product Engineering Division. In this role he is responsible for leading the global R&D of all Tata branded passenger cars, Trucks and Buses.</p>\n            <p>Dr. Tim Leverton joined Tata Motors in April 2010 as President, and Head of the Advanced and product Engineering Division. In this role he is responsible for leading the global R&D of all Tata branded passenger cars, Trucks and Buses.</p>\n            <p>Dr. Tim Leverton joined Tata Motors in April 2010 as President, and Head of the Advanced and product Engineering Division. In this role he is responsible for leading the global R&D of all Tata branded passenger cars, Trucks and Buses.</p> -->\n        </div>\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/spekar-details/spekar-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], SpekarDetailsPage);
    return SpekarDetailsPage;
}());

//# sourceMappingURL=spekar-details.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DelegatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_data__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_cache__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DelegatePage = /** @class */ (function () {
    function DelegatePage(navCtrl, navParams, restProvider, dataService, loading, screenOrientation, alertCtrl, cache) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.dataService = dataService;
        this.loading = loading;
        this.screenOrientation = screenOrientation;
        this.alertCtrl = alertCtrl;
        this.cache = cache;
        this.searchTerm = '';
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.cache.clearExpired();
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getDelegates();
        });
    }
    DelegatePage.prototype.getDelegates = function () {
        var _this = this;
        this.restProvider.getDelegateslist()
            .then(function (data) {
            _this.responseData = data;
            _this.delegateLists = _this.responseData.data;
            _this.arrayListSize = _this.delegateLists.length;
            _this.responseMessage = _this.responseData.message;
            _this.dataService.setItemsToSearch(_this.delegateLists);
            console.log(_this.delegateLists);
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            //this.presentToast(JSON.stringify(err));
        });
    };
    DelegatePage.prototype.setFilteredItems = function () {
        this.delegateLists = this.dataService.delegatesFilterItems(this.searchTerm);
    };
    DelegatePage.prototype.doPromptForNoLinkedinLink = function () {
        var prompt = this.alertCtrl.create({
            title: '',
            message: 'No link has been provided',
            cssClass: 'alertCustomCss',
            buttons: [
                {
                    text: 'Ok',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    DelegatePage.prototype.openLinkedIn = function (url) {
        window.open(url, '_system', 'location=yes');
    };
    DelegatePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DelegatePage');
    };
    DelegatePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    DelegatePage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__index_index__["a" /* IndexPage */]);
    };
    DelegatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-delegate',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/delegate/delegate.html"*/'<!--\n  Generated template for the DelegatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>delegate</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Delegate Profiles</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section class="profiles_section">\n    <div class="container">\n        <div class="profile_search">\n            <input type="text" name="" placeholder="Search" class="form-control" [(ngModel)]="searchTerm"  (input)="setFilteredItems($event)">\n            <button type="submit" (click)="setFilteredItems()"><i class="fa fa-search" aria-hidden="true" ></i></button>\n        </div>\n\n         <div class="delegate_list" *ngIf="!arrayListSize || arrayListSize == 0">\n                \n\n                    <div class="info">{{responseMessage}}</div>\n\n                   \n          </div>\n\n                        \n          <div *ngIf="arrayListSize || arrayListSize > 0">\n               \n            <div *ngFor="let delegateList of delegateLists">\n                <div class="delegate_list">\n                    <div class="img"><img data-src= "{{delegateList.user_image}}" onError="this.src=\'../assets/images/profile-img.jpg\';" alt=""></div>\n                    <div class="info">\n                        <h4>{{delegateList.display_name}}</h4>\n                        <h5>{{delegateList.user_designation}}</h5>\n                        <h5>{{delegateList.user_company}}</h5>\n                        <p *ngIf="delegateList.mobile_number"><span>Mobile: </span> {{delegateList.mobile_number}}</p>\n                        <p *ngIf="!delegateList.mobile_number"><span>Mobile: N/A</span></p>\n                        <p><span>E-mail:</span> {{delegateList.user_email}}</p>\n                    </div>\n                    <div class="btns">\n\n                         <div *ngIf="!delegateList.speaker_linkedin">\n                            <a (click) = "doPromptForNoLinkedinLink()" class="linkedin_btn" ><img src="../assets/images/linkedin-icon.png"></a>\n                        </div>\n\n                        <div *ngIf="delegateList.speaker_linkedin">\n                            <a (click)="openLinkedIn(delegateList.speaker_linkedin)" class="linkedin_btn" ><img src="../assets/images/linkedin-icon.png"></a>\n                        </div>\n\n                        <!-- <a href="#" class="save_btn">Save Contact</a> -->\n                    </div>\n                </div>\n\n            </div>\n\n        </div>\n\n        \n\n        <!-- <div class="delegate_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <h4>Mr. Manas Mondal</h4>\n                <h5>Compnay / organization</h5>\n                <p><span>E-mail:</span> manas.mondal@bluehorse.in</p>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n        </div>\n        <div class="delegate_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <h4>Mr. Manas Mondal</h4>\n                <h5>Compnay / organization</h5>\n                <p><span>E-mail:</span> manas.mondal@bluehorse.in</p>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n        </div>\n        <div class="delegate_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <h4>Mr. Manas Mondal</h4>\n                <h5>Compnay / organization</h5>\n                <p><span>E-mail:</span> manas.mondal@bluehorse.in</p>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n        </div>\n        <div class="delegate_list">\n            <div class="img"><img src="../assets/images/profile-img.jpg" alt=""></div>\n            <div class="info">\n                <h4>Mr. Manas Mondal</h4>\n                <h5>Compnay / organization</h5>\n                <p><span>E-mail:</span> manas.mondal@bluehorse.in</p>\n            </div>\n            <div class="btns">\n                <a href="#" class="linkedin_btn"><img src="../assets/images/linkedin-icon.png"></a>\n                <a href="#" class="save_btn">Save Contact</a>\n            </div>\n        </div> -->\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/delegate/delegate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_data_data__["a" /* DataProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6_ionic_cache__["b" /* CacheService */]])
    ], DelegatePage);
    return DelegatePage;
}());

//# sourceMappingURL=delegate.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EventPage = /** @class */ (function () {
    function EventPage(navCtrl, navParams, restProvider, loading, screenOrientation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.loading = loading;
        this.screenOrientation = screenOrientation;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getEvents();
        });
    }
    EventPage.prototype.getEvents = function () {
        var _this = this;
        this.restProvider.getEventlist()
            .then(function (data) {
            _this.responseData = data;
            _this.eventLists = _this.responseData.data;
            _this.arrayListSize = _this.eventLists.length;
            _this.responseMessage = _this.responseData.message;
            console.log(_this.eventLists);
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            //this.presentToast(JSON.stringify(err));
        });
    };
    EventPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventPage');
    };
    EventPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    EventPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__index_index__["a" /* IndexPage */]);
    };
    EventPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-event',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/event/event.html"*/'<!--\n  Generated template for the EventPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>event</ion-title>\n  </ion-navbar>\n\n</ion-header>\n -->\n \n <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Event Updates</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section class="event_section">\n    <div class="container">\n\n\n          <div class="event_list" *ngIf="!arrayListSize || arrayListSize == 0">\n                \n\n                    <div class="event_info">{{responseMessage}}</div>\n\n                   \n          </div>\n\n                        \n          <div *ngIf="arrayListSize || arrayListSize > 0">\n            <div *ngFor="let eventList of eventLists">\n                <div class="event_list">\n                    <!-- <div class="event_date">{{eventList.event_time}}<span>\n                    {{eventList.event_date}}</span>\n                    </div> -->\n                    \n                    <div class="event_info">\n                       {{eventList.event_excerpt}}\n                    </div>\n                </div>\n\n            </div>\n\n          </div>\n\n       \n      <!--   <div class="event_list">\n            <div class="event_date">14:20<span>22-10-18</span></div>\n            <div class="event_info">\n                Your tickets shall be emailed shortly to rour respective email ID.\n            </div>\n        </div>\n        <div class="event_list">\n            <div class="event_date">9:15<span>15-10-18</span></div>\n            <div class="event_info">\n                Your tickets shall be emailed shortly to rour respective email ID.\n            </div>\n        </div>-->\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/event/event.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], EventPage);
    return EventPage;
}());

//# sourceMappingURL=event.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItineraryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ItineraryPage = /** @class */ (function () {
    function ItineraryPage(navCtrl, navParams, restProvider, loading, screenOrientation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.loading = loading;
        this.screenOrientation = screenOrientation;
        this.pepperoni = false;
        this.sausage = false;
        this.mushrooms = false;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getInteneraryData();
        });
    }
    ItineraryPage.prototype.getInteneraryData = function () {
        var _this = this;
        this.restProvider.getItenerary()
            .then(function (data) {
            _this.responseData = data;
            _this.itenararyLists = _this.responseData.data;
            _this.arrayListSize = _this.itenararyLists.length;
            _this.responseMessage = _this.responseData.message;
            console.log(_this.responseData);
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            // this.presentToast(JSON.stringify(err));
        });
    };
    ItineraryPage.prototype.ionViewDidLoad = function () {
    };
    ItineraryPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    ItineraryPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__index_index__["a" /* IndexPage */]);
    };
    ItineraryPage.prototype.checkListSize = function (i, length) {
        console.log("check po " + i);
        //this.listSize = length;
    };
    ItineraryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-itinerary',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/itinerary/itinerary.html"*/'<!--\n  Generated template for the ItineraryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>itinerary</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Itinerary</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section>\n    <div class="container">\n\n            <div *ngFor="let itenararyList of itenararyLists ; let i = index" >\n\n                <div class="itinerary_list" *ngIf="itenararyLists.length>0">\n\n                        <div class="itinerary_title" style="margin-bottom:30px;">{{itenararyList.post_name}}<span>{{itenararyList.event_date}}</span></div>\n\n\n                        <div *ngFor="let itenararyDetails of itenararyList.schedule | slice:0:3">\n\n                            \n                        \n                            <ul class="itinerary_schedule" style="height:60px">\n                                <li>\n                                     <div class="col-5" style="min-height: 15px;">{{itenararyDetails.event_schedule_time}}\n                                        \n                                    </div> \n                                     <div class="col-7" style="line-height: 15px;height:20px;">\n                                       \n                                    {{itenararyDetails.event_schedule_description}}\n                                    </div> \n                                   \n                                </li>\n                               \n                            </ul>\n\n                        </div>\n\n                        <div *ngFor="let itenararyDetails of itenararyList.schedule | slice:3:listSize" >\n                        \n                            <ul *ngIf="itenararyList.value" class="itinerary_schedule" style="height:60px">\n                                <li>\n\n                                    <div class="col-5">{{itenararyDetails.event_schedule_time}}\n                                        \n                                    </div> \n                                     <div class="col-7" style="line-height: 15px;height:20px;">\n                                       \n                                    {{itenararyDetails.event_schedule_description}}\n                                    </div> \n\n\n                                     <!-- <div class="col-12">{{itenararyDetails.event_schedule_time}}<span>\n                                    {{itenararyDetails.event_schedule_description}}</span>\n                                    </div>  -->\n                                   \n                                </li>\n                               \n                            </ul>\n\n                        </div>\n\n                        <ion-toggle [(ngModel)]="itenararyList.value" *ngIf="itenararyList.schedule.length > 3" (ngModelChange) = "checkListSize(i,itenararyList.schedule.length)"></ion-toggle>\n\n                       \n                </div> \n\n            </div>\n\n       \n\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/itinerary/itinerary.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], ItineraryPage);
    return ItineraryPage;
}());

//# sourceMappingURL=itinerary.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index_index__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, navParams, alertCtrl, call, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.call = call;
        this.screenOrientation = screenOrientation;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    ContactPage.prototype.doPrompt = function () {
        var prompt = this.alertCtrl.create({
            title: '',
            message: 'Will be available on conference day',
            cssClass: 'alertCustomBlack',
            buttons: [
                {
                    text: 'Ok',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    ContactPage.prototype.callNumber = function (number) {
        this.call.callNumber(number, true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    ContactPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index_index__["a" /* IndexPage */]);
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/contact/contact.html"*/'<!--\n  Generated template for the ContactPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>contact</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Contact Us</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section>\n    <div class="container">\n        <div class="contact_banner"><img src="../assets/images/contact-img.jpg" alt=""></div>\n        <div class="contact_info">\n            <h4>You May Contact</h4>\n            <p>Tata Steel Limited</p>\n            <p>Tata Centre, 43 J.N.Road, Kolkata 700 071</p>\n            <p>Email: oemarketing@tatasteel.com</p>\n            <div class="row mt-3">\n                <div class="col-6 text-right border_right">\n                    <p>Mr. Animesh Sinha</p>\n                    <p>Phone: <a (click)="callNumber(\'8147060109\')" style="color:blue;">+91 81470 60109</a></p>\n                </div>\n                <div class="col-6 text-left">\n                    <p>Mr. Asit Kumar Mohanty</p>\n                    <p >Phone: <a (click) = "callNumber(\'9038095676\');"  style="color:blue;">+91 90380 95676</a></p>\n                </div>\n            </div>\n        </div>\n        <div class="feedback_link">\n            <img src="../assets/images/feedback-icon.png">\n            <span>Please Share your <a ng-href="" onclick="window.open(\'https://www.drivingsteel.com/feedback/\',\'_system\',\'location=yes\');"  style="color:blue;"> Feedback</a></span>\n        </div>\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/contact/contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__weather_weather__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_launch_navigator__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var OverviewPage = /** @class */ (function () {
    function OverviewPage(navCtrl, navParams, restProvider, platform, launchNavigator, screenOrientation, loading) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.platform = platform;
        this.launchNavigator = launchNavigator;
        this.screenOrientation = screenOrientation;
        this.loading = loading;
        this.pepperoni = false;
        this.aboutListData = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getOverView();
        });
    }
    OverviewPage.prototype.getOverView = function () {
        var _this = this;
        this.restProvider.getOverviewDetails()
            .then(function (data) {
            _this.responseData = data;
            _this.title = _this.responseData.data.title;
            // this.contentOne = this.responseData.data.content1;
            //  this.contentTwo = this.responseData.data.content2;
            // this.contentThree = this.responseData.data.content3;
            _this.aboutListData = _this.responseData.data;
            _this.arrayListSize = _this.aboutListData.length;
            _this.loader.dismiss();
            console.log("overview string " + JSON.stringify(_this.aboutListData));
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            // this.presentToast(JSON.stringify(err));
        });
    };
    OverviewPage.prototype.ionViewDidLoad = function () {
    };
    OverviewPage.prototype.loadMap = function () {
        var lat;
        var lng;
        var destination = lat + "," + lng;
        var options = {};
        this.launchNavigator.navigate(destination, options)
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator', error); });
    };
    OverviewPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    OverviewPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index_index__["a" /* IndexPage */]);
    };
    OverviewPage.prototype.gotoWeather = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__weather_weather__["a" /* WeatherPage */]);
    };
    OverviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-overview',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/overview/overview.html"*/'<!--\n  Generated template for the OverviewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>overview</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>{{title}}</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section>\n    <div class="container">\n\n                <div class="overview_section">\n\n      <div *ngFor="let aboutData of aboutListData |  slice:0:1">\n\n\n              <div class="overview_text">\n                  <!-- <p>{{aboutData}}</p> -->\n                  <div [innerHtml]="aboutData"></div>\n  			      </div>                \n\n         \n      </div>\n\n      <div *ngFor="let aboutData of aboutListData |  slice:1:arrayListSize">\n          \n              <!-- <p *ngIf="pepperoni">{{aboutData}}</p> -->\n              <div *ngIf="pepperoni" [innerHtml]="aboutData"></div>\n                 \n      </div>\n      \n\n       <div class="text-right"><ion-toggle [(ngModel)]="pepperoni"></ion-toggle></div>\n     </div>\n\n\n        <div class="overview_map"><a ng-href="" onclick="window.open(\'https://goo.gl/maps/TBpdviqnkUK2\',\'_system\',\'location=yes\');"><img src="../assets/images/map.png"></a></div>\n\n\n        <div class="overview_weather">\n            <img src="../assets/images/about-abudhabi-bg1.jpg" alt="">\n            <!-- <a (click) = "gotoWeather()" class="update_btn">Weather Updates</a> -->\n        </div>\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/overview/overview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* LoadingController */]])
    ], OverviewPage);
    return OverviewPage;
}());

//# sourceMappingURL=overview.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeatherPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__index_index__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WeatherPage = /** @class */ (function () {
    function WeatherPage(navCtrl, navParams, restProvider, storage, datePipe, screenOrientation, loading, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.storage = storage;
        this.datePipe = datePipe;
        this.screenOrientation = screenOrientation;
        this.loading = loading;
        this.toastCtrl = toastCtrl;
        this.weather = [];
        this.weatherA = [];
        this.weatherB = [];
        this.weatherC = [];
        this.weatherD = [];
        this.weatherE = [];
        this.weatherF = [];
        this.weatherG = [];
        this.weatherH = [];
        this.dateA = [];
        this.dateB = [];
        this.dateC = [];
        this.dateD = [];
        this.dateE = [];
        this.dateF = [];
        this.dateG = [];
        this.dateH = [];
        this.dateAf = [];
        this.dateBf = [];
        this.dateCf = [];
        this.dateDf = [];
        this.dateEf = [];
        this.dateFf = [];
        this.dateGf = [];
        this.dateHf = [];
        this.dateAfD = [];
        this.dateBfS = [];
        this.dateCfS = [];
        this.dateDfS = [];
        this.dateEfS = [];
        this.dateFfS = [];
        this.dateGfS = [];
        this.dateHfS = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getWeatherData();
        });
    }
    WeatherPage.prototype.getWeatherData = function () {
        var _this = this;
        console.log('ionViewDidLoad WeatherPage');
        this.restProvider.getWeather().subscribe(function (weatherRes) {
            _this.weather = weatherRes;
            _this.presentToast(JSON.stringify(_this.weather));
            //console.log(weather);
            //console.log(this.weather);
            _this.weatherA = _this.weather.daily.data[0];
            _this.weatherB = _this.weather.daily.data[1];
            _this.weatherC = _this.weather.daily.data[2];
            _this.weatherD = _this.weather.daily.data[3];
            _this.weatherE = _this.weather.daily.data[4];
            _this.weatherF = _this.weather.daily.data[5];
            _this.weatherG = _this.weather.daily.data[6];
            _this.weatherH = _this.weather.daily.data[7];
            //console.log(weather.daily.data);
            //console.log(this.date);
            _this.dateA = new Date(_this.weatherA.time * 1000);
            _this.dateAf = _this.datePipe.transform(_this.dateA, "dd");
            _this.dateAfD = _this.datePipe.transform(_this.dateA, "EEEE");
            _this.dateB = new Date(_this.weatherB.time * 1000);
            _this.dateBf = _this.datePipe.transform(_this.dateB, "dd");
            _this.dateBfS = _this.datePipe.transform(_this.dateB, "MMM yyyy");
            _this.dateC = new Date(_this.weatherC.time * 1000);
            _this.dateCf = _this.datePipe.transform(_this.dateC, "dd");
            _this.dateCfS = _this.datePipe.transform(_this.dateC, "MMM yyyy");
            _this.dateD = new Date(_this.weatherD.time * 1000);
            _this.dateDf = _this.datePipe.transform(_this.dateD, "dd");
            _this.dateDfS = _this.datePipe.transform(_this.dateD, "MMM yyyy");
            _this.dateE = new Date(_this.weatherE.time * 1000);
            _this.dateEf = _this.datePipe.transform(_this.dateE, "dd");
            _this.dateEfS = _this.datePipe.transform(_this.dateE, "MMM yyyy");
            _this.dateF = new Date(_this.weatherF.time * 1000);
            _this.dateFf = _this.datePipe.transform(_this.dateF, "dd");
            _this.dateFfS = _this.datePipe.transform(_this.dateF, "MMM yyyy");
            _this.dateG = new Date(_this.weatherG.time * 1000);
            _this.dateGf = _this.datePipe.transform(_this.dateG, "dd");
            _this.dateGfS = _this.datePipe.transform(_this.dateG, "MMM yyyy");
            _this.dateH = new Date(_this.weatherH.time * 1000);
            _this.dateHf = _this.datePipe.transform(_this.dateH, "dd");
            _this.dateHfS = _this.datePipe.transform(_this.dateH, "MMM yyyy");
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            _this.presentToast(JSON.stringify(err));
        });
    };
    WeatherPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    WeatherPage.prototype.ionViewDidLoad = function () {
    };
    WeatherPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    WeatherPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__index_index__["a" /* IndexPage */]);
    };
    WeatherPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-weather',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/weather/weather.html"*/'<!--\n  Generated template for the WeatherPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n  \n<ion-content>\n  <div class="header">\n      <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n      <h1>Weather Forecast</h1>\n      <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n  </div>\n\n      <ion-grid class="current_weather">  \n        <ion-row class="status">\n            <ion-col class="left">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherA.icon}}.png" alt=""></div>\n                {{weatherA.icon}}\n            </ion-col>\n            <ion-col class="right">\n            <span>{{this.dateAf}}</span>\n            {{this.dateAfD}}\n            </ion-col>\n        </ion-row>\n        <ion-row class="temareture">\n            <ion-col class="min-temp">\n             <span>{{weatherA.temperatureHigh}} &deg;F</span>\n             Max Temp\n          </ion-col>\n          <ion-col class="max-temp">\n                <span>{{weatherA.temperatureLow}} &deg;F</span>\n              Min Temp\n          </ion-col>\n        </ion-row>\n       </ion-grid>\n\n       <ion-grid class="list_weather">\n        <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateBf}}</span>\n                {{this.dateBfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherB.icon}}.png" alt=""></div>\n                {{weatherB.icon}}\n            </ion-col>\n            <ion-col class="temparature">\n              <span>{{weatherB.temperatureHigh}} &deg;F</span>\n               Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span> {{weatherB.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateCf}}</span>\n                {{this.dateCfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherC.icon}}.png" alt=""></div>\n                {{weatherC.icon}}\n                </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherC.temperatureHigh}} &deg;F</span>\n                Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span> {{weatherC.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateDf}}</span>\n                {{this.dateDfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherD.icon}}.png" alt=""></div>\n                {{weatherD.icon}}\n                </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherD.temperatureHigh}} &deg;F</span>\n                Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherD.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateEf}}</span>\n                {{this.dateEfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherE.icon}}.png" alt=""></div>\n                {{weatherE.icon}}\n                </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherE.temperatureHigh}} &deg;F</span>\n                Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherE.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateFf}}</span>\n                {{this.dateFfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherF.icon}}.png" alt=""></div>\n                {{weatherF.icon}}\n                </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherF.temperatureHigh}} &deg;F</span>\n                Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherF.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateGf}}</span>\n                {{this.dateGfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherG.icon}}.png" alt=""></div>\n                {{weatherG.icon}}\n                </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherG.temperatureHigh}} &deg;F</span>\n                Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherG.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col class="dates">\n                <span>{{this.dateHf}}</span>\n                {{this.dateHfS}}\n            </ion-col>\n            <ion-col class="status">\n                <div class=""><img src= "https://darksky.net/images/weather-icons/{{weatherH.icon}}.png" alt=""></div>\n                {{weatherH.icon}}\n                </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherH.temperatureHigh}} &deg;F</span>\n                Max Temp\n            </ion-col>\n            <ion-col class="temparature">\n                <span>{{weatherH.temperatureLow}} &deg;F</span>\n                Min Temp\n            </ion-col>\n        </ion-row>\n      </ion-grid>\n   \n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/weather/weather.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__angular_common__["d" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */]])
    ], WeatherPage);
    return WeatherPage;
}());

//# sourceMappingURL=weather.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_base64__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PhotosPage = /** @class */ (function () {
    function PhotosPage(navCtrl, navParams, restProvider, loading, camera, storage, toastCtrl, screenOrientation, base64, changeDetectorRef) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.loading = loading;
        this.camera = camera;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.screenOrientation = screenOrientation;
        this.base64 = base64;
        this.changeDetectorRef = changeDetectorRef;
        this.imageUpdateData = { data: { id: '', profile_image: '' } };
        this.toPushdata = { img_url: '' };
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.loader = this.loading.create({
            content: 'Please Wait...',
        });
        this.loader.present().then(function () {
            _this.getImages(false);
        });
        this.storage.get('user').then(function (result) {
            _this.userSession = result;
            _this.imageUpdateData.data.id = _this.userSession.id;
        });
    }
    PhotosPage.prototype.getImages = function (value) {
        var _this = this;
        this.restProvider.getGalleryImages()
            .then(function (data) {
            _this.responseData = data;
            _this.topLeft = _this.responseData.admin_gallery.top_left;
            _this.singleBig = _this.responseData.admin_gallery.single_big;
            _this.topMost = _this.responseData.admin_gallery.top_most;
            _this.mLeft = _this.responseData.admin_gallery.mid_left;
            _this.mRight = _this.responseData.admin_gallery.mid_right;
            _this.botBigOne = _this.responseData.admin_gallery.bottom_big;
            _this.smallOne = _this.responseData.admin_gallery.smaill_one;
            _this.smallTwo = _this.responseData.admin_gallery.small_two;
            _this.smallThree = _this.responseData.admin_gallery.small_three;
            _this.smallFour = _this.responseData.admin_gallery.small_four;
            _this.listImages = _this.responseData.data.user_gallery;
            console.log("response parse " + JSON.stringify(_this.responseData));
            _this.loader.dismiss();
            if (value == true)
                _this.presentToast(_this.dataResult.message);
        });
    };
    PhotosPage.prototype.getImage = function () {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.loader = _this.loading.create({
                content: 'Uploading...',
            });
            _this.loader.present().then(function () {
                _this.base64.encodeFile(imageData).then(function (base64File) {
                    _this.imageUpdateData.data.profile_image = base64File;
                    _this.toPushdata.img_url = imageData;
                    _this.uploadFile();
                    console.log(base64File);
                }, function (err) {
                    console.log(err);
                    //this.presentToast(err);
                    _this.loader.dismiss();
                });
            });
        }, function (err) {
            console.log("image error " + err);
            //this.presentToast(err);
        });
    };
    PhotosPage.prototype.uploadFile = function () {
        var _this = this;
        if (this.toPushdata.img_url.length > 0) {
            this.restProvider.uploadGalleryImage(this.imageUpdateData).then(function (result) {
                _this.dataResult = result;
                //this.toPushdata.img_url = this.dataResult.data.image_path;
                //this.listImages.push(this.toPushdata);
                //this.presentToast(JSON.stringify(this.listImages));
                //this.toPushdata.img_url = '';
                // this.changeDetectorRef.detectChanges();
                // this.loader.dismiss();
                if (_this.dataResult.success == true)
                    _this.getImages(true);
                else {
                    _this.loader.dismiss();
                    _this.presentToast(_this.dataResult.message);
                }
            }, function (err) {
                _this.loader.dismiss();
                //this.presentToast(err);
                _this.presentToast("Server error occured. Please try again");
                _this.toPushdata.img_url = '';
            });
        }
        else
            this.loader.dismiss();
    };
    PhotosPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    PhotosPage.prototype.ionViewDidLoad = function () {
    };
    PhotosPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PhotosPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__index_index__["a" /* IndexPage */]);
    };
    PhotosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-photos',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/photos/photos.html"*/'<!--\n  Generated template for the PhotosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>photos</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" media="screen">\n\n<ion-content cache-view="false">\n\n    <!-- <ion-refresher (ionRefresh)="clearCache($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher> -->\n\n    <div class="header">\n        <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n        <h1>Photos</h1>\n        <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n    </div>\n\n    <section class="green_bg">\n        <div class="gallery_section">\n            <div class="container">\n                <div class="row">\n                    <div class="col-6">\n                        <!-- <div class="upload_photo" (click) = "getImage()">\n                            <img  src="../assets/images/add-photo-icon.png" alt=" ">\n                            <span >Upload Photo</span>\n                        </div> -->\n\n                         <img class="gallery_lightbox" src="{{topLeft}}" alt="" imageViewer>\n\n                    </div>\n\n                    <div class="col-6">\n                        <img class="gallery_lightbox" src="{{topMost}}" alt="" imageViewer>\n                    </div>\n\n                    <div class="col-12">\n                        <img class="gallery_lightbox" src="{{singleBig}}" alt="" imageViewer>\n                    </div>\n\n                    <div class="col-6">\n                        <img class="gallery_lightbox" src="{{mLeft}}" alt="" imageViewer>\n                    </div>\n\n                    <div class="col-6">\n                        <img class="gallery_lightbox" src="{{mRight}}" alt="" imageViewer>\n                    </div>\n\n                    <div class="col-6">\n                        <img class="gallery_lightbox" src="{{botBigOne}}" alt="" imageViewer>\n                    </div>\n\n                    <div class="col-6">\n                        <div class="row">\n                            \n                            <div class="col-6">\n                                <img class="gallery_lightbox" src="{{smallOne}}" alt="" imageViewer>\n                            </div>\n\n                            <div class="col-6">\n                                <img class="gallery_lightbox" src="{{smallTwo}}"alt="" imageViewer>\n                            </div>\n\n                            <div class="col-6">\n                                <img class="gallery_lightbox" src="{{smallThree}}" alt="" imageViewer>\n                            </div>\n\n                            <div class="col-6">\n                                <img class="gallery_lightbox" src="{{smallFour}}" alt="" imageViewer>\n                            </div>\n\n                        </div>\n                    </div>\n\n                    <div *ngFor="let listImage of listImages">\n\n                          <div style="width:100%;">\n                                 <img class="gallery_lightbox" data-src= "{{listImage.img_url}}" alt="" imageViewer style="min-width:380px;"> \n\n                                <!-- <img-loader [src]="listImage.img_url" class="gallery_lightbox"  alt="" imageViewer style="min-width:380px;"></img-loader> -->\n\n                          </div>\n\n                    </div>\n                      \n                </div>\n            </div>\n        </div>\n    </section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/photos/photos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_base64__["a" /* Base64 */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */]])
    ], PhotosPage);
    return PhotosPage;
}());

//# sourceMappingURL=photos.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VirtualPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_launch_navigator__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_call_number__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var VirtualPage = /** @class */ (function () {
    function VirtualPage(navCtrl, navParams, restProvider, storage, alertCtrl, launchNavigator, screenOrientation, camera, toastCtrl, loading, call, base64) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.launchNavigator = launchNavigator;
        this.screenOrientation = screenOrientation;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.call = call;
        this.base64 = base64;
        this.passClicked = false;
        this.toSendData = { data: { id: '' } };
        this.imageUpdateData = { data: { id: '', profile_image: '' } };
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.storage.get('user').then(function (result) {
            _this.userSession = result;
            _this.userId = _this.userSession.id;
            _this.imageUpdateData.data.id = _this.userSession.id;
            _this.loader = _this.loading.create({
                content: 'Please Wait...',
            });
            _this.loader.present().then(function () {
                _this.getVirtualData();
            });
        });
    }
    VirtualPage.prototype.getVirtualData = function () {
        var _this = this;
        this.restProvider.getVirtualValues(this.userId).then(function (data) {
            _this.responseData = data;
            _this.downloadTicket = _this.responseData.data.download_ticket;
            _this.downloadRoomDetails = _this.responseData.data.download_room_detail;
            _this.downloadSittingArrangement = _this.responseData.data.sitting_arrangment_date;
            _this.passportImageUrl = _this.responseData.data.passport_image_url;
            console.log("" + _this.responseData);
            console.log("ticket res " + _this.downloadTicket);
            console.log("room res " + _this.downloadRoomDetails);
            console.log("sitting res " + _this.downloadSittingArrangement);
            _this.loader.dismiss();
            // this.presentToast(JSON.stringify(this.responseData));
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
            // this.presentToast("err : "+ err);
        });
    };
    VirtualPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VirtualPage');
    };
    VirtualPage.prototype.loadMap = function () {
        this.launchNavigator.navigate('West cornich, Abu Dhabi ,ARE')
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator', error); });
    };
    VirtualPage.prototype.doPrompt = function (type) {
        var msg;
        switch (type) {
            case "ticket":
                msg = this.downloadTicket;
                console.log("ticket" + msg);
                break;
            case "room":
                msg = this.downloadRoomDetails;
                console.log("room" + msg);
                break;
            case "sitting":
                msg = this.downloadSittingArrangement;
                console.log("sitting" + msg);
                break;
            default:
        }
        if (!msg || msg == "")
            msg = "Not Available";
        var prompt = this.alertCtrl.create({
            title: '',
            message: '' + msg,
            cssClass: 'alertCustomBlack',
            buttons: [
                {
                    text: 'Ok',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    VirtualPage.prototype.getImage = function () {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.loader = _this.loading.create({
                content: 'Uploading...',
            });
            _this.loader.present().then(function () {
                _this.base64.encodeFile(imageData).then(function (base64File) {
                    _this.imageUpdateData.data.profile_image = base64File;
                    _this.uploadFile();
                }, function (err) {
                    console.log(err);
                    //this.presentToast(err);
                    _this.loader.dismiss();
                });
            });
        }, function (err) {
            console.log("image error " + err);
            //this.presentToast(err);
        });
    };
    VirtualPage.prototype.uploadFile = function () {
        var _this = this;
        console.log("pass patload " + JSON.stringify(this.imageUpdateData));
        this.restProvider.uploadPassportImage(this.imageUpdateData).then(function (result) {
            _this.dataResult = result;
            _this.passportImageUrl = _this.dataResult.data.image_path;
            _this.loader.dismiss();
            _this.presentToast(_this.dataResult.message);
        }, function (err) {
            _this.loader.dismiss();
            //this.presentToast(err);
            _this.presentToast("Server error occured. Please try again");
        });
    };
    VirtualPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    VirtualPage.prototype.callNumber = function (number) {
        this.call.callNumber(number, true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    VirtualPage.prototype.presentImage = function () {
        console.log("clicked " + this.passportImageUrl);
        //this.photoViewer.show('https://www.freakyjolly.com/wp-content/
        //uploads/2017/08/cropped-fjlogo2.png');
        this.passClicked = !this.passClicked;
    };
    VirtualPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    VirtualPage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__index_index__["a" /* IndexPage */]);
    };
    VirtualPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-virtual',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/virtual/virtual.html"*/'<!--\n  Generated template for the VirtualPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>virtual</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<div class="header">\n    <a (click)="goBack()" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>\n    <h1>Virtual Concierge</h1>\n    <a (click)="gotoIndex()" class="home_btn"><i class="fa fa-home" aria-hidden="true"></i></a>\n</div>\n\n<section>\n    <div class="container">\n\n        <!-- <div class="virtual_item left">\n            \n                <img src="../assets/images/upload-passport.png" alt="">\n                <span><a (click) = "getImage()">Upload</a></span> <span> <a (click) = "presentImage();">Passport</a></span>\n            \n        </div> -->\n\n        <div class="virtual_item left upload_passport">\n            <span>\n                <a (click) = "getImage()"> <img src="../assets/images/upload-passport.png" alt="">\n               Upload</a>\n                <a (click) = "presentImage()" style="color: grey;">Passport</a>\n            </span>\n        </div>\n        \n\n        <div class="col-12"  *ngIf="passClicked">\n                    <img class="gallery_lightbox" src="{{passportImageUrl}}" alt="" imageViewer>\n        </div>\n\n        <div class="virtual_item right">\n            <a (click)="doPrompt(\'ticket\')">\n                <img src="../assets/images/download-ticket.png" alt="">\n                <span>Download Ticket</span>\n            </a>\n        </div>\n        <div class="virtual_item left">\n            <a (click)="doPrompt(\'room\')">\n                <img src="../assets/images/download-room-details.png" alt="">\n                <span>Room Details</span>\n            </a>\n        </div>\n        \n        <div class="virtual_item right">\n            <a (click)="doPrompt(\'sitting\')">\n                <img src="../assets/images/sitting-arrangements.png" alt="">\n                <span>Seating Arrangement</span>\n            </a>\n        </div>\n\n        <div class="address_section row">\n\n            <div class="address_details col-6">\n                <h5>Grand Hyatt</h5>\n                <p>Address:</p>\n                <p>West cornich, Abu Dhabi United Arab Emirates</p>\n                <p>Phone: <a (click)="callNumber(\'+97125101234\')" style="color:grey;">(+97) 12510 1234</a></p>\n                <p>abudhabi.grand@hyatt.com</p>\n            </div>\n\n            <!-- <div class="map col-6"><img src="../assets/images/map2.png" alt=""></div> -->\n\n           <div class="map col-6" ><a ng-href="" onclick="window.open(\'https://goo.gl/maps/TBpdviqnkUK2\',\'_system\',\'location=yes\');"><img src="../assets/images/map2.png" alt=""></a></div>\n\n            <div class="emergency_contact col-12">\n                <h5>Emergency Contact</h5>\n\n                <p>Asit Mohanty: <a (click)="callNumber(\'9038095676\')" style="color:grey;">(+91) 90380 95676</a> / <span><a (click)="callNumber(\'+971563900425\')" style="color:grey;">(+97) 15639 00425</a></span></p>\n\n                <p>Vamsi Goutam: <a (click)="callNumber(\'9225509992\')" style="color:grey;">(+91) 92255 09992</a> / <span><a (click)="callNumber(\'+971505183691\')" style="color:grey;">(+97) 15051 83691</a></span></p>\n\n            </div>\n        </div>\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/virtual/virtual.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__["a" /* Base64 */]])
    ], VirtualPage);
    return VirtualPage;
}());

//# sourceMappingURL=virtual.js.map

/***/ }),

/***/ 169:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 169;

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__about_about__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profile_profile__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__speaker_speaker__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__delegate_delegate__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__event_event__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__itinerary_itinerary__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__contact_contact__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__overview_overview__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__photos_photos__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__virtual_virtual__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_in_app_browser__ = __webpack_require__(222);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var IndexPage = /** @class */ (function () {
    function IndexPage(navCtrl, navParams, storage, screenOrientation, splashScreen, statusBar, iab) {
        //this.splashScreen.show();
        //this.statusBar.styleDefault();
        //this.statusBar.show();
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.screenOrientation = screenOrientation;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.iab = iab;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    IndexPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IndexPage');
        //this.splashScreen.show();
        //this.statusBar.show();
        this.statusBar.styleDefault();
        this.splashScreen.hide();
    };
    IndexPage.prototype.gotoAbout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__about_about__["a" /* AboutPage */]);
    };
    IndexPage.prototype.gotoProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__profile_profile__["a" /* ProfilePage */]);
    };
    IndexPage.prototype.gotoSpeaker = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__speaker_speaker__["a" /* SpeakerPage */]);
    };
    IndexPage.prototype.gotoDelegate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__delegate_delegate__["a" /* DelegatePage */]);
    };
    IndexPage.prototype.gotoEvent = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__event_event__["a" /* EventPage */]);
    };
    IndexPage.prototype.gotoItinerary = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__itinerary_itinerary__["a" /* ItineraryPage */]);
    };
    IndexPage.prototype.gotoContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__contact_contact__["a" /* ContactPage */]);
    };
    IndexPage.prototype.gotoOverview = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__overview_overview__["a" /* OverviewPage */]);
    };
    IndexPage.prototype.gotoPhotos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__photos_photos__["a" /* PhotosPage */]);
    };
    IndexPage.prototype.gotoVirtual = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__virtual_virtual__["a" /* VirtualPage */]);
    };
    IndexPage.prototype.openDrivingSteel = function () {
        this.iab.create('https://www.drivingsteel.com/');
    };
    IndexPage.prototype.gotoLogin = function () {
        var _this = this;
        this.storage.set('isLogIn', false).then(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        });
    };
    IndexPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-index',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/index/index.html"*/'<!--\n  Generated template for the IndexPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>index</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n\n<ion-content>\n\n<section class="dashboard_section">\n    <div class="container">\n        <div class="row">\n            <div class="col-7 col-lg-4">\n                <a (click)="gotoAbout();" class="dash_item">\n                    <img src="../assets/images/about.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-5 col-lg-4">\n                <a (click)="gotoOverview();" class="dash_item">\n                    <img src="../assets/images/overview.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-6 col-lg-4">\n                <a (click)="gotoSpeaker();" class="dash_item">\n                    <img src="../assets/images/speaker.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-6 col-lg-4">\n                <a (click)="gotoDelegate();" class="dash_item">\n                     <img src="../assets/images/delegte.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-6 col-lg-4">\n                <a (click)="gotoEvent();" class="dash_item">\n                    <img src="../assets/images/event.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-6 col-lg-4">\n                <a (click)="gotoItinerary();" class="dash_item">\n                    <img src="../assets/images/itinerary.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-5 col-lg-3">\n                <a (click)="gotoProfile();" class="dash_item">\n                    <img src="../assets/images/profile.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-7 col-lg-3">\n                <a (click)="gotoPhotos();" class="dash_item">\n                    <img src="../assets/images/gallery.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-6 col-lg-3">\n                <a (click)="gotoVirtual();" class="dash_item">\n                    <img src="../assets/images/virtual.jpg" alt="">\n                </a>\n            </div>\n            <div class="col-6 col-lg-3">\n                <a (click)="gotoContact();" class="dash_item">\n                    <img src="../assets/images/contact.jpg" alt="">\n                </a>\n            </div>\n            \n        </div>\n        <div class="landing_footer">\n            <div class="left">\n                <a (click)="gotoLogin();">Sign Out</a> | <a ng-href="" onclick="window.open(\'https://www.drivingsteel.com/privacy-policy/\',\'_system\',\'location=yes\');">Privacy Policy</a>\n            </div>\n            <div class="right">\n                <a ng-href="" onclick="window.open(\'https://www.drivingsteel.com/\',\'_system\',\'location=yes\');">www.drivingsteel.com</a>\n            </div>\n        </div>\n    </div>\n</section>\n\n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/index/index.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_14__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_17__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], IndexPage);
    return IndexPage;
}());

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 212:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		449,
		14
	],
	"../pages/contact/contact.module": [
		452,
		13
	],
	"../pages/delegate/delegate.module": [
		453,
		12
	],
	"../pages/event/event.module": [
		454,
		11
	],
	"../pages/index/index.module": [
		455,
		10
	],
	"../pages/itinerary/itinerary.module": [
		456,
		9
	],
	"../pages/overview/overview.module": [
		457,
		8
	],
	"../pages/photos/photos.module": [
		458,
		7
	],
	"../pages/pregistration/pregistration.module": [
		459,
		6
	],
	"../pages/profile/profile.module": [
		460,
		5
	],
	"../pages/speaker/speaker.module": [
		461,
		4
	],
	"../pages/spekar-details/spekar-details.module": [
		462,
		3
	],
	"../pages/virtual/virtual.module": [
		463,
		2
	],
	"../pages/weather/weather.module": [
		464,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 212;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RestProvider = /** @class */ (function () {
    function RestProvider(http) {
        this.http = http;
        //apiUrl = "http://bluehorse.in/driving_steel/jsonapi/";
        this.apiUrl = "https://www.drivingsteel.com/jsonapi/";
        this.weatherApiKey = '20e9a5701b8393aaba56c9bcbf3f7b57';
        this.lan = '24.4539'; //22.5726
        this.lat = '54.3773'; //88.3639
        console.log('Hello RestProvider Provider');
        //this.http.clearCookies();
        this.weatherUrl = 'https://api.darksky.net/forecast/' + this.weatherApiKey + '/' + this.lan + ',' + this.lat;
    }
    RestProvider.prototype.getSpeakerlist = function () {
        var _this = this;
        this.mathValue = Math.random();
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'speaker.php' + '?hash_id=' + _this.mathValue).subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getEventlist = function () {
        var _this = this;
        this.mathValue = Math.random();
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'event_updates.php' + '?hash_id=' + _this.mathValue).subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getDelegateslist = function () {
        var _this = this;
        this.mathValue = Math.random();
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'delegate.php' + '?hash_id=' + _this.mathValue).subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getGalleryImages = function () {
        var _this = this;
        this.mathValue = Math.random();
        console.log("math value " + this.mathValue);
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'gallery.php' + '?hash_id=' + _this.mathValue)
                .map(function (results) { return results; })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getItenerary = function () {
        var _this = this;
        this.mathValue = Math.random();
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'itinerary.php' + '?hash_id=' + _this.mathValue).subscribe(function (data) {
                console.log("gallery data " + data);
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getOverviewDetails = function () {
        var _this = this;
        this.mathValue = Math.random();
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'abu_dhabi.php' + '?hash_id=' + _this.mathValue).subscribe(function (data) {
                console.log("overview data " + data);
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getVirtualValues = function (userId) {
        var _this = this;
        this.mathValue = Math.random();
        console.log("virtual payload " + this.apiUrl + 'virtual_concierge.php' + '?hash_id=' + this.mathValue + "&id=" + userId);
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.apiUrl + 'virtual_concierge.php' + '?hash_id=' + _this.mathValue + "&id=" + userId)
                .subscribe(function (res) {
                resolve(res);
                console.log(res);
            }, function (err) {
                resolve(err);
            });
        });
    };
    RestProvider.prototype.getAboutusDetails = function () {
        var _this = this;
        this.mathValue = Math.random();
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'about.php' + '?hash_id=' + _this.mathValue).subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    //Following function takes city and state as an input
    RestProvider.prototype.getWeather = function () {
        this.mathValue = Math.random();
        return this.http.get(this.weatherUrl + '?hash_id=' + this.mathValue)
            .map(function (result) { return result; });
    };
    RestProvider.prototype.loginNew = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'login.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.SignUp = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'register.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.profileUpdate = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'profile.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.forgetPassword = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'forgotpassword.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.uploadProfileImage = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'profile_photo.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                console.log("image data up " + JSON.stringify(res));
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.uploadGalleryImage = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'upload_phto.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                console.log("image gallery up " + JSON.stringify(res));
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.uploadPassportImage = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            };
            _this.http.post(_this.apiUrl + 'upload_passport.php', JSON.stringify(credentials), httpOptions)
                .subscribe(function (res) {
                console.log("image data up " + JSON.stringify(res));
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(292);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_img_viewer__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_about_about__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_speaker_speaker__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_spekar_details_spekar_details__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_delegate_delegate__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_event_event__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_itinerary_itinerary__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_contact_contact__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_overview_overview__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_photos_photos__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_virtual_virtual__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_weather_weather__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_pregistration_pregistration__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__angular_common_http__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_data_data__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_transfer__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_launch_navigator__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__angular_common__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_call_number__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_in_app_browser__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35_ionic_cache__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_base64__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37_ionic_image_loader__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_fcm__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_index_index__["a" /* IndexPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_speaker_speaker__["a" /* SpeakerPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_spekar_details_spekar_details__["a" /* SpekarDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_delegate_delegate__["a" /* DelegatePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_event_event__["a" /* EventPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_itinerary_itinerary__["a" /* ItineraryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_overview_overview__["a" /* OverviewPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_photos_photos__["a" /* PhotosPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_virtual_virtual__["a" /* VirtualPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_weather_weather__["a" /* WeatherPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_pregistration_pregistration__["a" /* PregistrationPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/delegate/delegate.module#DelegatePageModule', name: 'DelegatePage', segment: 'delegate', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/event/event.module#EventPageModule', name: 'EventPage', segment: 'event', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/index/index.module#IndexPageModule', name: 'IndexPage', segment: 'index', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/itinerary/itinerary.module#ItineraryPageModule', name: 'ItineraryPage', segment: 'itinerary', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/overview/overview.module#OverviewPageModule', name: 'OverviewPage', segment: 'overview', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/photos/photos.module#PhotosPageModule', name: 'PhotosPage', segment: 'photos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pregistration/pregistration.module#PregistrationPageModule', name: 'PregistrationPage', segment: 'pregistration', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker/speaker.module#SpeakerPageModule', name: 'SpeakerPage', segment: 'speaker', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/spekar-details/spekar-details.module#SpekarDetailsPageModule', name: 'SpekarDetailsPage', segment: 'spekar-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/virtual/virtual.module#VirtualPageModule', name: 'VirtualPage', segment: 'virtual', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/weather/weather.module#WeatherPageModule', name: 'WeatherPage', segment: 'weather', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_23__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_26__angular_forms__["f" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_img_viewer__["a" /* IonicImageViewerModule */],
                __WEBPACK_IMPORTED_MODULE_35_ionic_cache__["a" /* CacheModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_37_ionic_image_loader__["a" /* IonicImageLoader */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_index_index__["a" /* IndexPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_speaker_speaker__["a" /* SpeakerPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_spekar_details_spekar_details__["a" /* SpekarDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_delegate_delegate__["a" /* DelegatePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_event_event__["a" /* EventPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_itinerary_itinerary__["a" /* ItineraryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_overview_overview__["a" /* OverviewPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_photos_photos__["a" /* PhotosPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_virtual_virtual__["a" /* VirtualPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_weather_weather__["a" /* WeatherPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_pregistration_pregistration__["a" /* PregistrationPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_22__providers_rest_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_24__providers_data_data__["a" /* DataProvider */],
                Storage,
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_camera__["a" /* Camera */],
                Window,
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_31__angular_common__["d" /* DatePipe */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_base64__["a" /* Base64 */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_fcm__["a" /* FCM */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_screen_orientation__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_fcm__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//import { CacheService } from "ionic-cache";
//import { ImageLoaderConfig } from 'ionic-image-loader';

var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storage, screenOrientation, fcm, toastCtrl, alertCtrl) {
        var _this = this;
        this.storage = storage;
        this.screenOrientation = screenOrientation;
        this.fcm = fcm;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        platform.ready().then(function () {
            _this.screenOrientation.lock(_this.screenOrientation.ORIENTATIONS.PORTRAIT);
            // this.cache.enableCache(false);
            //this.cache.setDefaultTTL(1);
            //  `this.cache.clearExpired();
            //this.imageLoaderConfig.enableDebugMode();
            // this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
            // this.imageLoaderConfig.setFallbackUrl('assets/imgs/logo.png');
            // this.imageLoaderConfig.setMaximumCacheAge(1);
            _this.storage.ready().then(function () {
                _this.storage.get('isLogIn').then(function (result) {
                    console.log("islogin", result);
                    _this.fcm.subscribeToTopic('user');
                    _this.fcm.getToken().then(function (token) {
                        _this.storage.set('userToken', token).then(function () {
                            //this.presentToast(token);
                        });
                    });
                    _this.fcm.onNotification().subscribe(function (data) {
                        //this.presentToast(JSON.stringify(data));
                        if (data.wasTapped) {
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
                        }
                        else {
                            _this.fcmResponse = data;
                            var prompt_1 = _this.alertCtrl.create({
                                title: _this.fcmResponse.title,
                                message: _this.fcmResponse.body,
                                cssClass: 'alertCustomBlack',
                                buttons: [
                                    {
                                        text: 'Ok',
                                        handler: function (data) {
                                            console.log('Cancel clicked');
                                        }
                                    }
                                ]
                            });
                            prompt_1.present();
                        }
                    });
                    if (result != null) {
                        if (result == true)
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_index_index__["a" /* IndexPage */];
                        else
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
                    }
                    else
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
                    //statusBar.styleDefault();
                    //splashScreen.hide();              
                });
            });
        });
    }
    MyApp.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 20000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_fcm__["a" /* FCM */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index_index__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pregistration_pregistration__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//import { ScreenOrientation } from '@ionic-native/screen-orientation';


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, restProvider, toastCtrl, loading, storage, alertCtrl, splashScreen, statusBar) {
        //this.splashScreen.show();
        //this.statusBar.styleDefault();
        //this.statusBar.show();
        var _this = this;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.loginData = { data: { username: '', password: '', remember: '0', userToken: '' } };
        this.forgetPasswordData = { data: { email: '' } };
        //this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.group = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            UserName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].email]),
            Password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6)])
        });
        this.storage.get('userToken').then(function (result) {
            _this.loginData.data.userToken = result;
        });
    }
    HomePage.prototype.doLogin = function () {
        var _this = this;
        console.log(this.loginData);
        var loader = this.loading.create({
            content: 'Please Wait...',
        });
        loader.present().then(function () {
            _this.restProvider.loginNew(_this.loginData).then(function (result) {
                _this.responseData = result;
                loader.dismiss();
                _this.checkLoginResult();
            }, function (err) {
                loader.dismiss();
                _this.presentToast("This is an error" + err);
                console.log("login eror", JSON.stringify(err));
            });
        });
    };
    HomePage.prototype.checkLoginResult = function () {
        var _this = this;
        console.log(this.responseData.data);
        this.presentToast(this.responseData.message);
        if (this.responseData.success == true) {
            this.storage.set('user', this.responseData.data).then(function () {
                _this.storage.set('isLogIn', true);
                _this.gotoIndex();
            });
        }
        else {
            this.loginData.data.username = "";
            this.loginData.data.password = "";
        }
    };
    HomePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.doPrompt = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Forget Password!!!',
            message: "Enter your E-Mail id here",
            cssClass: 'alertCustomCss',
            inputs: [
                {
                    name: 'email',
                    placeholder: 'E-Mail'
                },
            ],
            buttons: [
                {
                    text: 'Submit',
                    handler: function (data) {
                        var validateObj = _this.validateEmail(data);
                        if (!validateObj.isValid) {
                            prompt.setMessage(validateObj.message);
                            return false;
                        }
                        else {
                            _this.forgetPasswordData.data.email = data.email;
                            console.log('Submit clicked', _this.forgetPasswordData);
                            _this.sendForgetpassworddata();
                        }
                    }
                },
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage.prototype.validateEmail = function (data) {
        if (/(.+)@(.+){2,}\.(.+){2,}/.test(data.email)) {
            return {
                isValid: true,
                message: ''
            };
        }
        else {
            return {
                isValid: false,
                message: 'Email address is required'
            };
        }
    };
    HomePage.prototype.sendForgetpassworddata = function () {
        var _this = this;
        var loader = this.loading.create({
            content: 'Please Wait...',
        });
        loader.present().then(function () {
            _this.restProvider.forgetPassword(_this.forgetPasswordData).then(function (result) {
                _this.responseData = result;
                loader.dismiss();
                _this.presentToast(_this.responseData.message);
            }, function (err) {
                loader.dismiss();
                _this.presentToast(err);
                console.log(err);
            });
        });
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DelegatePage');
        //this.splashScreen.show();
        //this.statusBar.show();
        this.statusBar.styleDefault();
        this.splashScreen.hide();
    };
    HomePage.prototype.gotoRegistration = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pregistration_pregistration__["a" /* PregistrationPage */]);
    };
    HomePage.prototype.gotoIndex = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__index_index__["a" /* IndexPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/home/home.html"*/'<!-- <ion-header>\n  <ion-navbar>\n    <ion-title>\n      Ionic Blank\n    </ion-title>\n  </ion-navbar>\n</ion-header> -->\n\n<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">\n<link rel="stylesheet" type="text/css" href="../assets/css/style.css">\n\n<ion-content>\n\n<div class="container">\n<div class="top_logo"><img src="../assets/images/tata-steel-logo.png" alt=""></div>\n<div class="login_section">\n    <div class="logo"><img src="../assets/images/logo.png" alt=""></div>\n    <form [formGroup]="group">\n        \n        <div class="form-group">\n            <input type="email" name="username" placeholder="Email ID" class="form-control" [(ngModel)]="loginData.data.username" formControlName="UserName">\n\n            <span class="error" *ngIf="group.get(\'UserName\').hasError(\'required\') && group.get(\'UserName\').touched" style="margin-top: 10px;margin-left: 5px">\n                  Email is required\n            </span>\n\n            <span class="error" *ngIf="group.get(\'UserName\').hasError(\'email\') && group.get(\'UserName\').touched" style="margin-top: 10px;margin-left: 5px">\n                  Please enter valid email\n            </span>\n        </div>\n\n      \n\n        <div class="form-group">\n            <input type="password" name="password" placeholder="Password" class="form-control" [(ngModel)]="loginData.data.password" formControlName="Password">\n       \n                 \n            <span class="error" *ngIf="group.get(\'Password\').hasError(\'required\') && group.get(\'Password\').touched" style="margin-top: 10px;margin-left: 5px">\n              Password is required\n            </span>\n            <span class="error" *ngIf="group.get(\'Password\').hasError(\'minlength\') && group.get(\'Password\').touched" style="margin-top: 10px;margin-left: 5px">\n              Password must be at least 6 characters long.\n            </span>\n      \n        </div>\n\n        <div class="login_btns">\n            <!-- <button type="button" onclick="location.href=\'index.html\'">Login</button> -->\n			<button (click)="doLogin();" [disabled]="group.invalid">Login</button>\n            <p><a (click)="doPrompt()">Forgot Password?</a></p>\n            <p><a (click)="gotoRegistration()">SignUp</a></p>\n        </div>\n    </form>\n</div>\n\n<div class="login_footer">Abu Dhabi | 2018</div>\n</div>\n  \n</ion-content>\n'/*ion-inline-end:"/home/kritika/AtrijitNewIonic/DrivingSteel/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[284]);
//# sourceMappingURL=main.js.map